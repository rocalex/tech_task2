import os
import random
import io
import xml.etree.ElementTree as ET
import tensorflow as tf
import pandas as pd

from os import listdir
from shutil import copyfile
from collections import namedtuple
from parse import *
from PIL import Image
from object_detection.utils import dataset_util, label_map_util


def generate_label_map(labels_xml):
    labels = []
    for i, child in enumerate(labels_xml):
        attributes = child.find('./attributes')
        if attributes:
            for j, el in enumerate(attributes):
                for val in el.find('values').text.split('\n'):
                    labels.append('{},{},{}'.format(child.find('name').text, el.find('name').text, val))
        else:
            labels.append(child.find('name').text)

    label_map_string = ''
    for idx, label in enumerate(labels):
        item = f"""
item {{
    id: {idx + 1:d}
    name: '{label}'
}}
"""
        label_map_string = label_map_string + item

    with open('annotations/label_map.pbtxt', 'w') as file:
        file.write(label_map_string)


def xml_to_csv(filename_list, xml_root):
    xml_list = []
    for filename in filename_list:
        index = parse('{}.jpg', filename)[0]
        el = xml_root.find('.//image[@id="{}"]'.format(index))
        if el:
            width = el.attrib['width']
            height = el.attrib['height']
            for member in el.findall('box'):
                attributes = member.findall('attribute')
                if len(attributes) > 0:
                    for attribute in attributes:
                        value = (
                            filename,
                            width,
                            height,
                            '{},{},{}'.format(member.attrib['label'], attribute.attrib['name'], attribute.text),
                            int(float(member.attrib['xtl'])),
                            int(float(member.attrib['ytl'])),
                            int(float(member.attrib['xbr'])),
                            int(float(member.attrib['ybr']))
                        )
                        xml_list.append(value)
                else:
                    value = (
                        filename,
                        width,
                        height,
                        member.attrib['label'],
                        int(float(member.attrib['xtl'])),
                        int(float(member.attrib['ytl'])),
                        int(float(member.attrib['xbr'])),
                        int(float(member.attrib['ybr']))
                    )
                    xml_list.append(value)

    column_name = ['filename', 'width', 'height',
                   'class', 'xmin', 'ymin', 'xmax', 'ymax']
    xml_df = pd.DataFrame(xml_list, columns=column_name)
    return xml_df


def class_text_to_int(row_label):
    label_map = label_map_util.load_labelmap('annotations/label_map.pbtxt')
    label_map_dict = label_map_util.get_label_map_dict(label_map)
    return label_map_dict[row_label]


def create_tf_example(group, path):
    with tf.compat.v1.gfile.GFile(os.path.join(path, '{}'.format(group.filename)), 'rb') as fid:
        encoded_jpg = fid.read()
    encoded_jpg_io = io.BytesIO(encoded_jpg)
    image = Image.open(encoded_jpg_io)
    width, height = image.size

    filename = group.filename.encode('utf8')
    image_format = b'jpg'
    xmins = []
    xmaxs = []
    ymins = []
    ymaxs = []
    classes_text = []
    classes = []

    for index, row in group.object.iterrows():
        xmins.append(row['xmin'] / width)
        xmaxs.append(row['xmax'] / width)
        ymins.append(row['ymin'] / height)
        ymaxs.append(row['ymax'] / height)
        classes_text.append(row['class'].encode('utf8'))
        classes.append(class_text_to_int(row['class']))

    tf_example = tf.compat.v1.train.Example(features=tf.compat.v1.train.Features(feature={
        'image/height': dataset_util.int64_feature(height),
        'image/width': dataset_util.int64_feature(width),
        'image/filename': dataset_util.bytes_feature(filename),
        'image/source_id': dataset_util.bytes_feature(filename),
        'image/encoded': dataset_util.bytes_feature(encoded_jpg),
        'image/format': dataset_util.bytes_feature(image_format),
        'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
        'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
        'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
        'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
        'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
        'image/object/class/label': dataset_util.int64_list_feature(classes),
    }))
    return tf_example


def split(df, group):
    data = namedtuple('data', ['filename', 'object'])
    gb = df.groupby(group)
    return [data(filename, gb.get_group(x)) for filename, x in zip(gb.groups.keys(), gb.groups)]


def generate_tfrecord(data, root, path, output_path):
    writer = tf.compat.v1.python_io.TFRecordWriter(output_path)
    examples = xml_to_csv(data, root)
    grouped = split(examples, 'filename')
    for group in grouped:
        tf_example = create_tf_example(group, path)
        writer.write(tf_example.SerializeToString())
    writer.close()


def main(_):
    # Make label map for TensorFlow
    tree = ET.parse('dataset/annotations.xml')
    root = tree.getroot()

    labels = root.find('./meta/task/labels')

    # Generate label map
    generate_label_map(labels)

    # Divide data into train and test
    for f in listdir('images/train'):
        os.remove(os.path.join('images/train', f))
    for f in listdir('images/test'):
        os.remove(os.path.join('images/test', f))

    images = listdir('dataset/images')
    random.shuffle(images)
    train_data = images[:(len(images) - len(images) // 10)]
    test_data = images[(len(images) - len(images) // 10):]

    for file_name in train_data:
        copyfile(os.path.join('dataset/images', file_name), os.path.join('images/train', file_name))

    for file_name in test_data:
        copyfile(os.path.join('dataset/images', file_name), os.path.join('images/test', file_name))

    # Generate TFRecord
    generate_tfrecord(train_data, root, os.path.join('images/train'), 'annotations/train.record')
    generate_tfrecord(test_data, root, os.path.join('images/test'), 'annotations/test.record')


if __name__ == '__main__':
    tf.compat.v1.app.run()
