# Task 2 PPE

## Training the Model

```shell
$ python partition_data.py
$ python model_main_tf2.py --model_dir=models/my_faster_rcnn_resnet50_v1_coco17 --pipeline_config_path=models/my_faster_rcnn_resnet50_v1_coco17/pipeline.config
```

## Mornitor Training Job Progress using TensorBoard

```shell
$ tensorboard --logdir=models/my_faster_rcnn_resnet50_v1_coco17
```

## Exporting a Trained Model

```shell
$ python exporter_main_v2.py --input_type image_tensor --pipeline_config_path models/my_faster_rcnn_resnet50_v1_coco17/pipeline.config --trained_checkpoint_dir models/my_faster_rcnn_resnet50_v1_coco17 --output_directory exported_models/my_model
```